'''
    *** DiceRollParser ***

    DiceRollParser parse your litteral expressions as rolls of dice, and returns a score and a detailed score
    eg: '3d6+3-1d4'
    
    Expressions can contain strictly positive integers, operators ('+', '-'), and dices 'XdX' (where X can be any positive integer)
    Expressions should begin and end with an strictly positive integer
    
    Examples of use:
    
    drp = DiceRollParser("3d6+8")
    drp.roll()
    print( "result is {}".format( drp.score ) )
    print( "detailed result is {}".format( drp.detail ) )
    
    Or
    
    drp = DiceRollParser.roll_this("3d6+8")
    print( "result is {}".format( drp.score ) )
    print( "detailed result is {}".format( drp.detail ) )
    
    
    Invalid expressions will raise a ValueError
        
@author: olinox14
'''

import random
import re


_HISTORY = []
def clear_history():
    """ clear the history of the last rolls """
    global _HISTORY
    _HISTORY = []

_POSITION = 0
    
def _randint(value):
    """return a random number between 1 and value"""
    return random.randint(1, value)

class DiceRollParser():
    """parse a litteral expression and interpret it as a roll of dice
    eg: '3d6+3-1d4'
    """
    _VALIDATION_RE = r"^(([0-9]+(d[1-9]+)?\)?)[\+\-]?)*([0-9]+(d*[1-9]+)?)$"

    def __init__(self, expr, name = ""):
        self._expr = ""
        self.expr = expr
        self._score = None
        self._detail = []
        self._name = ""
        self.name = name

    def __repr__(self):
        return "DiceRollParser('{}') >> {}   ({})".format( self.expr, self.score, self.detail ) \
            if self._score != None \
            else "DiceRollParser > not rolled"

    #properties
    @property
    def expr(self):
        """the current expression to parse"""
        return self._expr

    @expr.setter
    def expr(self, expr):
        """set a new expression to parse"""
        global _HISTORY
        global _POSITION
        _HISTORY.insert(0, expr)
        _POSITION = 0
        if not self.__class__.is_valid(expr):
            raise ValueError("'{}' is not a valid expression for DiceRollParser expression".format(expr))
        self._expr = expr

    @property
    def score(self):
        """return the score of the last roll"""
        return self._score

    @property
    def detail(self):
        """return the detailed score of the last roll"""
        return " ".join(self._detail)

    @property
    def name(self):
        """name of the current DiceRollParser (optional)"""
        return self._name

    @name.setter
    def name(self, name):
        """set a name for the DiceRollParser (optional)"""
        self._name = str(name)

    #class methods
    @classmethod
    def roll_die(cls, faces_number):
        """return a random result between 1 and faces_number
        (like a real die would do)"""
        try:
            return _randint(faces_number)
        except (TypeError, ValueError):
            raise ValueError("{} is not a valid number of faces for a dice".format(faces_number))

    @classmethod
    def is_valid(cls, expr):
        """return true if 'expr' can be interpreted as a roll of dice"""
        return re.match( cls._VALIDATION_RE, cls._normalize( expr ) ) != None

    @classmethod
    def _normalize(cls, expr):
        return expr.replace(" ","").lower()

    @classmethod
    def roll_this(cls, expr):
        """returns a DiceRollParser object"""
        drp = cls(expr)
        drp.roll()
        return drp

    @classmethod
    def previous(cls):
        """return the previous expr used with a DiceRollParser
        THIS DOES NOT MODIFY THE CURRENT expr VARIABLE!"""
        global _POSITION
        if _POSITION < (len(_HISTORY) - 1):
            _POSITION += 1
        return _HISTORY[_POSITION]

    @classmethod
    def next(cls):
        """return the previous expr used with a DiceRollParser
        THIS DOES NOT MODIFY THE CURRENT expr VARIABLE!"""
        global _POSITION
        if _POSITION > 0:
            _POSITION -= 1
        return _HISTORY[_POSITION]

    # methods
    def roll(self):
        """Rolls the dices!
        This will update 'score' and 'detail' """
        self._score = 0
        self._detail = []
        
        # normalize
        expr = self.__class__._normalize(self._expr)

        # parse
        parsed_expr = re.findall(r"[\w']+|[\W']+", expr)

        def _eval_member(member):
            evaluated = re.findall(r"[\d]+", member)
            try:
                str_number, str_faces = evaluated
                total_score, detailed_score = 0, []
                for _ in range(0, int(str_number) ):
                    score = self.__class__.roll_die( int(str_faces) )
                    total_score += score
                    detailed_score.append( str(score) )
                self._detail.append( "[{}]".format( ",".join( detailed_score ) ) )
                return str( total_score )

            except ValueError:
                self._detail.append(member)
                return member

        #evaluate
        evaluated_expr = []
        for member in parsed_expr:
            evaluated_expr.append( _eval_member( member ) )

        self._score = eval( "".join(evaluated_expr) )
        return self._score



if __name__ == '__main__':
    print("Roll your dices")
    in_expr = input("> ")
    in_drp = DiceRollParser.roll_this(in_expr)
    print( "Result: {} ({})".format( in_drp.score, in_drp.detail ) )
