** DiceRollParser **

[GNU License](https://bitbucket.org/dmonde/dicerollparser/src/5a8a0d54a9877b1185c36118327a81a5c25b7cf8/LICENSE.txt?at=master&fileviewer=file-view-default)

DiceRollParser interprets your literal expressions as rolls of dice  
*Eg: '3d6 + 3-1d4'*  
    
Expressions can contain strictly positive integers, operators ('+', '-'), and dices 'XdX' (where X can be any positive integer)  
Expressions should begin and end with an strictly positive integer  
    
Examples of use:  
 
	from DiceRollParser import DiceRollParser

    drp = DiceRollParser("3d6+8")  
    drp.roll()  

    print( "result is {}".format( drp.score() ) )  
    print( "detailed result is {}".format( drp.detail() ) )

Or

	from DiceRollParser import DiceRollParser

    drp = DiceRollParser.roll_this("3d6+8")  

    print( "result is {}".format( drp.score() ) )  
    print( "detailed result is {}".format( drp.detail() ) )

 
    
> Invalid expressions will raise a `ValueError`

#### CONFIGURATION

DiceRollParser has been developped and test for python 3.4

#### CONTRIBUTION

Any opinion / contribution is welcome

#### TO INSTALL

Simply copy the DiceRollParser.py file in your \PythonXX\Lib directory




