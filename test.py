'''
Created on 18 nov. 2016

@author: olinox
'''
import unittest

from DiceRollParser import DiceRollParser, clear_history


class Test(unittest.TestCase):
    """unitests for DiceRollParser"""
    def test_validity(self):
        """check the expression validity control"""

        # check valid expressions
        self.assertTrue( DiceRollParser.is_valid("3") )
        self.assertTrue( DiceRollParser.is_valid("0d6") )
        self.assertTrue( DiceRollParser.is_valid("1d6") )
        self.assertTrue( DiceRollParser.is_valid("1d66") )
        self.assertTrue( DiceRollParser.is_valid("66666d66666") )
        self.assertTrue( DiceRollParser.is_valid("1d4+2") )
        self.assertTrue( DiceRollParser.is_valid("1d4-2") )
        self.assertTrue( DiceRollParser.is_valid("1d4 - 2 ") )
        self.assertTrue( DiceRollParser.is_valid("6d102+123") )
        self.assertTrue( DiceRollParser.is_valid("6d102+123+8d6") )
        self.assertTrue( DiceRollParser.is_valid("6d102+123+8d6+2+1+1-8-6d4+8-2d101+100d2") )
        self.assertTrue( DiceRollParser.is_valid("32+3-0-2") )
        self.assertTrue( DiceRollParser.is_valid("1d1-3") )
        self.assertTrue( DiceRollParser.is_valid("1000000000000d10000000000000") )
        self.assertTrue( DiceRollParser.is_valid("1000000000000 d 10000000000000") )

        # test invalid expressions
        self.assertFalse( DiceRollParser.is_valid("") )
        self.assertFalse( DiceRollParser.is_valid("d6" ) )
        self.assertFalse( DiceRollParser.is_valid("1d" ) )
        self.assertFalse( DiceRollParser.is_valid("abc" ) )
        self.assertFalse( DiceRollParser.is_valid("1d6 * 2" ) )
        self.assertFalse( DiceRollParser.is_valid("16/4" ) )
        self.assertFalse( DiceRollParser.is_valid("1d2,3" ) )
        self.assertFalse( DiceRollParser.is_valid("1d2.3" ) )
        self.assertFalse( DiceRollParser.is_valid("1d6 +- 2" ) )
        self.assertFalse( DiceRollParser.is_valid("1d0" ) )
        self.assertFalse( DiceRollParser.is_valid("1d-8" ) )

    def test_repr(self):
        """test the __repr__ function"""
        drp = DiceRollParser("0")
        self.assertEqual( drp.__repr__(), "DiceRollParser > not rolled" )

        drp.roll()
        self.assertEqual( drp.__repr__(), "DiceRollParser('0') >> 0   (0)" )

        drp.expr = "1d1+3"
        drp.roll()
        self.assertEqual( drp.__repr__(), "DiceRollParser('1d1+3') >> 4   ([1] + 3)" )

        drp.expr = "3d1+3"
        drp.roll()
        self.assertEqual( drp.__repr__(), "DiceRollParser('3d1+3') >> 6   ([1,1,1] + 3)" )

        drp.expr = "0d1"
        drp.roll()
        self.assertEqual( drp.__repr__(), "DiceRollParser('0d1') >> 0   ([])" )


    def test_score(self):
        """ test the score getter and setter """
        drp = DiceRollParser("111d1")

        self.assertEqual(drp.score, None)

        drp.roll()

        self.assertEqual(drp.score, 111)

        def _set_score():
            drp.score = 3
        self.assertRaises(AttributeError, _set_score )

    def test_name(self):
        """ test name get and set """
        drp = DiceRollParser("0")
        self.assertEqual(drp.name, "")

        drp.name = "foo"
        self.assertEqual(drp.name, "foo")

        drp.name = 123
        self.assertEqual(drp.name, "123")

    def test_roolthis(self):
        """test that roll_thiss class function returns the same thing that classic process """
        drp1 = DiceRollParser.roll_this("3d1+3")
        drp2 = DiceRollParser("3d1+3")
        drp2.roll()
        self.assertEqual( drp1.__repr__(), drp2.__repr__() )

    def test_expr(self):
        """test expr get and set"""
        drp = DiceRollParser("0")

        self.assertEqual(drp.expr, "0")

        drp.expr = "1d6"
        self.assertEqual(drp.expr, "1d6")

        def _set_expr():
            drp.expr = "invalid"
        self.assertRaises(ValueError, _set_expr)

    def test_rolldie(self):
        """test the roll_die class method"""
        result = DiceRollParser.roll_die(100)
        self.assertTrue( result in range(1,101) )
        self.assertRaises(ValueError, DiceRollParser.roll_die, "abcd")
        self.assertRaises(ValueError, DiceRollParser.roll_die, "0")
        self.assertRaises(ValueError, DiceRollParser.roll_die, "-5")

    def test_previous_next(self):
        """test the previous and next class function"""
        clear_history()

        drp = DiceRollParser("1d1")
        drp.expr = "1d2"
        drp.expr = "1d3"
        drp.expr = "1d4"

        # test the previous function
        self.assertEqual( drp.expr, "1d4" )
        self.assertEqual( DiceRollParser.previous(), "1d3" )
        # current expr does not change:
        self.assertEqual( drp.expr, "1d4" )
        self.assertEqual( DiceRollParser.previous(), "1d2" )
        self.assertEqual( DiceRollParser.previous(), "1d1" )
        # if history has reach the first expr, stays there:
        self.assertEqual( DiceRollParser.previous(), "1d1" )

        # test the next function
        self.assertEqual( DiceRollParser.next(), "1d2" )
        self.assertEqual( DiceRollParser.next(), "1d3" )
        self.assertEqual( DiceRollParser.next(), "1d4" )
        # if history has reach the latest expr, stays there:
        self.assertEqual( DiceRollParser.next(), "1d4" )

        # test the set of a new expr while browsing history
        # > it should go back to position 0 in the history
        self.assertEqual( DiceRollParser.previous(), "1d3" )
        self.assertEqual( DiceRollParser.previous(), "1d2" )

        drp.expr = "1d5"
        self.assertEqual( drp.expr, "1d5" )
        self.assertEqual( DiceRollParser.previous(), "1d4" )
        self.assertEqual( DiceRollParser.previous(), "1d3" )
 
    def test_roll(self):
        """can not be tested, so we just check that no error is raised..."""
        drp = DiceRollParser("14d14")
        drp.roll()
        drp.roll()
        drp.roll()


if __name__ == "__main__":
    unittest.main()
    